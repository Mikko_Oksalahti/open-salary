# Palkat esiin - frontend code

This project includes the source code for the Palkat esiin -service frontend.

Written in typescript and javascript. Uses React, Redux and Chart.js. For a more detailed list of libraries used, see package.json.

The service is available in [https://palkat-esiin.bitbucket.io/](https://palkat-esiin.bitbucket.io/)

## How to build

If you want to build the project yourself, you'll need:

* git
* node / npm
* some editor (e.g. Visual Studio Code)

Once these have been installed, clone the repo and run:

    npm install

Then start the page on your default browser with:

    npm start

To run unit tests, type:

    npm run test

## Some notes about the code

Currently, these things bug me the most:

* CSS files and styles in general are a bit messy
* Still missing some unit tests

Will try to improve these over time.

## About backend code

Backend basically has only ~100 lines of code. Due to its nature, that code is not made public. Doing so would defy its own purpose.

## License

This source code is released under [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.html).

(c) 2021, Mikko Oksalahti
