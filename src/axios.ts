import Axios, { AxiosInstance } from 'axios';

export const AWS_API_URL : string = 'https://0eexmpx93m.execute-api.eu-central-1.amazonaws.com/default/open-salary-pos';
export const FIREBASE_API_URL : string = 'https://open-salary-38508-default-rtdb.europe-west1.firebasedatabase.app';

const instance : AxiosInstance = Axios.create({
    // baseURL: 'https://open-salary-38508-default-rtdb.europe-west1.firebasedatabase.app/'
});

export default instance;