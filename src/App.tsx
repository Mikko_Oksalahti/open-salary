import React, { useEffect, useCallback, Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './containers/Home/Home';
import SideBar from './containers/SideBar/SideBar';
import withErrorHandler from './hoc/withErrorHandler/withErrorHandler';
import axios from './axios';
import { fetchOccupations } from './store/reducers/reducer';
import { useAppDispatch } from './store/hooks';

const SalarySearch : any = React.lazy(() => {
  return import('./containers/SalarySearch/SalarySearch');
});

const SubmitSalary : any = React.lazy(() => {
  return import('./containers/SubmitSalary/SubmitSalary');
});

const Statistics : any = React.lazy(() => {
  return import('./containers/Statistics/Statistics');
});

function App() {

  const dispatch = useAppDispatch();
  const initOccupationList =  useCallback(() => dispatch(fetchOccupations()), [dispatch]);

  useEffect(() => {
      initOccupationList();
  }, [initOccupationList]);

  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className="App">
        <Suspense fallback={<p></p>}>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/salary-search' render={props => <SalarySearch {...props} />} />
            <Route path='/submit-salary' render={props => <SubmitSalary {...props} />} />
            <Route path='/statistics' render={props => <Statistics {...props} />} />
          </Switch>
        </Suspense>
        <SideBar />
      </div>
    </BrowserRouter>
  );
}

export default withErrorHandler(App, axios);
