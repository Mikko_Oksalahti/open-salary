import React from 'react';
import { NavLink } from 'react-router-dom';
import Button from '../../UI/Button/Button';

const SubmitStepCongratulations = () => {
    return (
        <div className='narrow-text-box left-justified'>
            <div className='fancy-title'>Kiitos!</div>
            <p>Palkkatietosi on nyt lisätty palveluun.</p>
            <p>Jos valitsit olemassa olevan ammattinimikkeen, tietosi ovat heti mukana tilastoissa. Jos lisäsit uuden ammattinimikkeen, tietosi tulevat mukaan tilastoihin heti kun olemme tarkastaneet ammattinimikkeen.</p>
            <p>Kiitos kun kerroit palkkasi. Siitä on apua kaikille, jotka haluavat tietää ammattinsa oikean palkkatason.</p>
            <p>Voit ilmoittaa palkkasi aina kun se muuttuu tai palata vaikka vuoden päästä uudelleen ilmoittamaan sen, vaikka se olisikin pysynyt samana.</p>
            <div className='centered t-margin-medium'>
                <NavLink to='/'>
                    <Button>Takaisin aloitussivulle</Button>
                </NavLink>
            </div>
        </div>
    )
}

export default SubmitStepCongratulations;