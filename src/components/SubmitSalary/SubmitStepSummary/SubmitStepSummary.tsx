import React from 'react';
import classes from './SubmitStepSummary.module.css';

interface ISubmitFormSummary {
    salary : string,
    occupation : string,
    experience : string,
    gender : string,
    province : string
}

const SubmitFormSummary = (props : ISubmitFormSummary) => {
    return (
        <React.Fragment>
        <div className='narrow-text-box left-justified'>
            <p className='fancy-title'>Yhteenveto</p>
            <p>Tarkasta syöttämäsi tiedot ja palaa takaisin jos haluat vielä muuttaa jotain.</p>
            <p>Kun olet valmis, paina <strong>Lähetä</strong> nappia.</p>
            <table className={classes.SubmitStepSummary}>
                <tbody>
                    <tr><td>Palkka:</td><td><strong>{props.salary}&euro;</strong> / kk</td></tr>
                    <tr><td>Ammatti:</td><td><strong>{props.occupation}</strong></td></tr>
                    <tr><td>Työkokemus:</td><td><strong>{props.experience}</strong> vuotta</td></tr>
                    <tr><td>Sukupuoli:</td><td><strong>{props.gender}</strong></td></tr>
                    <tr><td>Maakunta:</td><td><strong>{props.province}</strong></td></tr>
                </tbody>
            </table>
        </div>
        </React.Fragment>
    );
}

export default SubmitFormSummary;