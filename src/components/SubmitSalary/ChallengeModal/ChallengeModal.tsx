import React, { useEffect, useState } from 'react';
import Modal from '../../UI/Modal/Modal';
import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';
import axios, { AWS_API_URL } from '../../../axios';
import { ISubmitData } from '../../../containers/SubmitSalary/SubmitSalary';

import challengePicLoading from '../../../images/challenge-pic-loading.gif';

import classes from './ChallengeModal.module.css';
import { ILooseObject } from '../../../store/types';

interface IChallengeModalProps {
    submitData : ISubmitData,
    occupations : ILooseObject,
    close : () => void,
    done : () => void
}

const ChallengeModal = (props : IChallengeModalProps) => {

    const [ sending, setSending ] = useState(false);
    const [ loading, setLoading ] = useState(false);
    const [ retry, setRetry ] = useState(false);
    const [ pics, setPics ] = useState(["", "", "", ""]);
    const [ answer, setAnswer ] = useState('');

    const fetchChallengeFromBackend = () => {
        // Get challenge images from backend
        setLoading(true);
        setPics(["", "", "", ""]);

        axios.get(AWS_API_URL + '?challenge=' + Date.now())
        .then(response => {
            setLoading(false);
            if(response.data) {
                if(response.data.status === 'ok' && response.data.pics) {
                    setPics([
                                response.data.pics.p1,
                                response.data.pics.p2,
                                response.data.pics.p3,
                                response.data.pics.p4
                            ]);
                } else {
                    // TODO: Handle error in UI
                    console.log('error');
                }
            } else {
                // TODO: Handle error in UI
                console.log('no data');
            }
        })
        .catch(error => {
            // TODO: Handle error in UI
            setLoading(false);
            console.log(error);
        });
    }

    useEffect(() => {
        fetchChallengeFromBackend();
    }, []);

    const sendSalaryToBackend = () => {
        setSending(true);

        // Form the salary object
        let salaryPost = {
            challengeanswer: answer,
            salary: {
                salary: parseInt(props.submitData.salary),
                occupation: props.submitData.occupation,
                experience: parseInt(props.submitData.experience),
                gender: props.submitData.gender,
                province: props.submitData.province,
                occupationName: ''
            }
        }
        if(props.submitData.occupation === 'newocc') {
            salaryPost.salary.occupationName = props.occupations[props.submitData.occupation];
        }

        // Send salary to backend
        axios.post(AWS_API_URL + '?addsalary',
            salaryPost)
            .then(response => {
                if(response.data) {
                    if(response.data.status === 'ok') {
                        setSending(false);
                        setRetry(false);
                        props.done();
                    } else if(response.data.status === 'challenge failed') {
                        setSending(false);
                        setRetry(true);
                        fetchChallengeFromBackend();
                    } else {
                        // TODO: Handle error in UI
                        setSending(false);
                        console.log('error');
                    }
                }
            })
            .catch(error => {
                // TODO: Handle error in UI
                setSending(false);
                console.log(error);
            });
    }

    return (
        <Modal cancelModal={props.close}>
            {retry ? 
                <div className={['centered', 'fancy-title', classes.AlertColor].join(' ')}>Yritä uudestaan</div>
                 : 
                <div className='centered fancy-title'>Melkein valmista</div>
            }
            <p className='t-margin-small'>Varmistetaan vielä, että olet ihminen. Kerro mikä näiden summa on:</p>
            {loading || sending ? (
                    <div className='centered'>
                        {sending ? 
                            (
                                <div className={[classes.ChallengeSpinner, classes.ChallengeSending].join(' ')}>Lähetetään...</div>
                            ) : 
                            (
                                <div className={classes.ChallengeSpinner}><img alt='' className={classes.ChallengeLoading} src={challengePicLoading}/></div>
                            )
                        }
                    </div>
                ) : (
                    <React.Fragment>
                        <div className={['centered', classes.ChallengePicsRow].join(' ')}>
                            <img alt='kuva' className={classes.ChallengePics} src={pics[0]}/>
                            <img alt='kuva' className={classes.ChallengePics} src={pics[1]}/>
                        </div>
                        <div className={['centered', classes.ChallengePicsRow].join(' ')}>
                            <img alt='kuva' className={classes.ChallengePics} src={pics[2]}/>
                            <img alt='kuva' className={classes.ChallengePics} src={pics[3]}/>
                        </div>
                    </React.Fragment>
                )
            }
            <div className='centered t-margin-small'>
                <Input 
                    required
                    type="number" 
                    autoFocus 
                    small
                    placeholder='summa'
                    value={answer}
                    onChange={(e : any) => setAnswer(e.target.value)}>
                </Input>
                &nbsp;&nbsp;
                <Button disabled={answer.length === 0 || sending || loading} onClick={sendSalaryToBackend}>Lähetä palkkatietoni</Button>
            </div>
        </Modal>
    );
}

export default ChallengeModal;