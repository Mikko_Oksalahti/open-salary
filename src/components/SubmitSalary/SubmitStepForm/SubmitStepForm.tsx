import React from 'react';
import Input from '../../UI/Input/Input';
import Select from '../../UI/Select/Select';
import { ILooseObject } from '../../../store/types'
import InfoIconWithHoverPopup from '../../UI/InfoIconWithHoverPopup/InfoIconWithHoverPopup';

interface ISubmitStepFormProps {
    occupations : ILooseObject,
    genders : ILooseObject,
    provinces : ILooseObject,
    salary : string,
    occupation : string,
    experience : string,
    gender : string,
    province : string,
    setSalary : (salary : string) => void,
    occupationSelected : (occupation : string) => void,
    setExperience : (experience : string) => void,
    setGender : (gender : string) => void,
    setProvince : (province : string) => void,
}

const SubmitStepForm = (props : ISubmitStepFormProps) => {

    const occupationOptions = Object.keys(props.occupations).map((occ, index) => (
        <option key={occ} value={occ}>{props.occupations[occ]}</option>
    ));

    const genderOptions = Object.keys(props.genders).map((gen, index) => (
        <option key={gen} value={gen}>{props.genders[gen]}</option>
    ));

    const provinceOptions = Object.keys(props.provinces).map((prov, index) => (
        <option key={prov} value={prov}>{props.provinces[prov]}</option>
    ));

    return (
        <div className='narrow-text-box left-justified'>
            <p className='fancy-title'>Syötä palkkatietosi</p>
            <form autoComplete="off">
                <label 
                    htmlFor='salary' 
                    className='input-info'>
                    Kuukausipalkka (&euro; / brutto)
                </label>
                <Input 
                    autoFocus
                    type='number'
                    small
                    value={props.salary}
                    onChange={(e : any) => props.setSalary(e.target.value)}
                    id='salary'/>
                <InfoIconWithHoverPopup><p>Ilmoita <strong>täysiaikainen</strong> kuukausipalkkasi ilman veroja, sisältäen mahdollisen bonuksen kuukausiosuuden</p></InfoIconWithHoverPopup>
                
                <label 
                    htmlFor='occupation' 
                    className='input-info'>
                    Ammatti
                </label>
                <Select 
                    value={props.occupation} 
                    onChange={(e : any) => props.occupationSelected(e.target.value)}
                    id='occupation'>
                    {occupationOptions}
                </Select>

                <label 
                    htmlFor='experience' 
                    className='input-info'>
                    Työkokemus tällä <strong>ammattinimikkeellä</strong> (vuosina)
                </label>
                <Input 
                    type='number'
                    value={props.experience}
                    onChange={(e : any) => props.setExperience(e.target.value)}
                    small 
                    id='experience'/>
                <InfoIconWithHoverPopup><p>Kerro miten kauan olet tehnyt tätä työtä, vaikka olisitkin ollut usealla työnantajalla</p></InfoIconWithHoverPopup>

                <label 
                    htmlFor='gender' 
                    className='input-info'>
                    Sukupuoli
                </label>
                <Select 
                    value={props.gender} 
                    onChange={(e : any) => props.setGender(e.target.value)}
                    id='gender'
                    style={{maxWidth:'40rem'}}>
                    {genderOptions}
                </Select>
                <InfoIconWithHoverPopup><p>Sinun ei tarvitse ilmoittaa sukupuoltasi mutta palkkatietoja voidaan vertailla sukupuolten välillä paremmin jos ilmoitat. Suosittelemmekin siksi, että ilmoitat :)</p></InfoIconWithHoverPopup>

                <label 
                    htmlFor='province' 
                    className='input-info'>
                    Maakunta
                </label>
                <Select 
                    value={props.province} 
                    onChange={(e : any) => props.setProvince(e.target.value)}
                    id='province' 
                    style={{maxWidth:'40rem'}}>
                    {provinceOptions}
                </Select>
            </form>
        </div>
    );
}

export default SubmitStepForm;