import React from 'react';

import classes from './SubmitStepInstructions.module.css';

const SubmitStepInstructions = () => {
    return (
        <React.Fragment>
            <div className={[classes.SubmitStepInstructions, 'narrow-text-box', 'left-justified'].join(' ')}>
                <p className='fancy-title'>Ohjeet</p>
                <p>Ilmoita palkkatietosi mahdollisimman totuudenmukaisesti niin tiedoista on hyötyä sinulle ja muille käyttäjille.</p>
                <p>Voit ilmoittaa palkkasi aina kun se muuttuu tai jos aloitat uudessa paikassa.</p>
                <p>Tämä palvelu ei kerää mitään tietoja, minkä perusteella sinut voisi tunnistaa.</p>
                <p>Pari yksityiskohtaa:</p>
                <ul>
                    <li>Palvelu kerää täysiaikaisten työntekijöiden palkkatietoja. Jos teet osa-aikaista työtä, ilmoita mitä palkkasi olisi, jos olisit täysipäiväisesti töissä.</li>
                    <li>Jos saat esim. vuosittaista bonusta palkan lisäksi, lisää sen kuukausiosuus ilmoittamaasi palkkaan.</li>
                </ul>
            </div>
        </React.Fragment>
    );     
}

export default SubmitStepInstructions;