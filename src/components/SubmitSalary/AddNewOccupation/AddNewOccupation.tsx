import React, { useState } from 'react';
import Modal from '../../UI/Modal/Modal';
import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';

interface IAddOccupationProps {
    close : () => void,
    addOccupation : (occupation : string) => void
}

const AddNewOccupation = (props : IAddOccupationProps) => {

    const [ newOccupation, setNewOccupation ] = useState<string>('');

    return (
        <Modal cancelModal={props.close}>
            <div className='centered fancy-title'>Lisää ammatti?</div> 
            <div 
                className='b-margin-small t-margin-small'>
                Voit lisätä uuden ammattinimikkeen jos olet varma, että sinun ammattiasi ei löytynyt listalta. Uusi nimike tulee mukaan tilastoihin kun olemme tarkastaneet sen.
            </div>
            <Input 
                maxLength={100}
                value={newOccupation} 
                onChange={(e : any) => setNewOccupation(e.target.value)}
                autoFocus 
                placeholder='syötä uusi ammattinimike'>
            </Input>
            <div className='centered t-margin-medium'>
                <div className='on-left'>
                    <Button 
                        secondary
                        onClick={props.close}>
                        <div>
                            Kumoa
                        </div>
                    </Button>
                </div>
                <div className='on-right'>
                    <Button 
                        disabled={newOccupation.length === 0}
                        onClick={() => props.addOccupation(newOccupation)}>
                        <div>
                            Lisää
                        </div>
                    </Button>
                </div>
            </div>
        </Modal>
    )
}

export default AddNewOccupation;