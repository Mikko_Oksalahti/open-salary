import { NavLink } from 'react-router-dom';

import classes from './AppLogo.module.css';

const AppLogo = () => {
    return (
        <NavLink to='/' className={classes.AppLogo}>
            <span>Palkat esiin</span>
        </NavLink>
    );
}

export default AppLogo;