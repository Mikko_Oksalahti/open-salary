import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import SalarySummary from "./SalarySummary";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders correct summary", () => {

  // empty data
  act(() => {
    render(<SalarySummary data={[]}/>, container);
  });
  expect(container.textContent).toContain("Keskipalkka (brutto): 0 € / kk");
  expect(container.textContent).toContain("Vaihteluväli: 0 € - 0 € / kk");
  expect(container.textContent).toContain("Ilmoituksia: 0 kpl");

  // realistic data

  const realData1 = JSON.parse('[{"experience":20,"gender":"male","occupation":"sofarc","province":"uusimaa","salary":6100,"timestamp":1615920864333},{"experience":2,"gender":"male","occupation":"sofarc","province":"uusimaa","salary":6258,"timestamp":1615985814933},{"experience":6,"gender":"male","occupation":"sofarc","province":"pirkanmaa","salary":4876,"timestamp":1616150854889}]');

  act(() => {
    render(<SalarySummary data={realData1} />, container);
  });
  expect(container.textContent).toContain("Keskipalkka (brutto): 6100 € / kk");
  expect(container.textContent).toContain("Vaihteluväli: 4876 € - 6258 € / kk");
  expect(container.textContent).toContain("Ilmoituksia: 3 kpl");

  const realData2 = JSON.parse('[{"experience":3,"gender":"male","occupation":"sofdevman","province":"uusimaa","salary":6500,"timestamp":1615922896662},{"experience":8,"gender":"male","occupation":"sofdevman","province":"uusimaa","salary":6150,"timestamp":1616005142608},{"experience":5,"gender":"male","occupation":"sofdevman","province":"uusimaa","salary":5500,"timestamp":1616150087491},{"experience":1,"gender":"male","occupation":"sofdevman","province":"uusimaa","salary":6500,"timestamp":1616766506747}]');

  act(() => {
    render(<SalarySummary data={realData2} />, container);
  });
  expect(container.textContent).toContain("Keskipalkka (brutto): 6325 € / kk");
  expect(container.textContent).toContain("Vaihteluväli: 5500 € - 6500 € / kk");
  expect(container.textContent).toContain("Ilmoituksia: 4 kpl");
  
});