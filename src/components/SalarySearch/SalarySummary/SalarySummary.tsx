import React, { useEffect, useState } from 'react';
import InfoIconWithHoverPopup from '../../UI/InfoIconWithHoverPopup/InfoIconWithHoverPopup';
import { getOccupationSalarySummaryData, ISalaryData } from '../../../statistics/statFunctions';

import classes from './SalarySummary.module.css';

interface ISalarySummaryProps {
    data : ISalaryData[],
    occupation : string
}

const SalarySummary = (props : ISalarySummaryProps) => {

    const [ medianSalary, setMedianSalary ] = useState<number>(0);
    const [ salaryRange, setSalaryRange ] = useState<string>('');
    const [ count, setCount ] = useState<number>(0);

    useEffect(() => {
        const summaryStats = getOccupationSalarySummaryData(props.data);
        setCount(summaryStats.count);
        setSalaryRange(summaryStats.salaryRange);
        setMedianSalary(summaryStats.medianSalary);
    }, [props.data]);

    return props.data ? (
        <div className={[classes.SalaryInfoCard, 'left-justified', 't-margin-medium', 'b-margin-medium'].join(' ')}>
            <div>
                <div className={classes.Occupation}>{props.occupation}</div>
            </div>
            <div>
                <span >Keskipalkka (brutto): </span>
                <span className={classes.MedianValue}>{medianSalary} &euro; / kk</span>
                <InfoIconWithHoverPopup><p>Keskipalkka on palkkojen mediaani, eli keskimmäinen arvo. Ei keskiarvo.</p></InfoIconWithHoverPopup>
            </div>
            <div>
                <span>Vaihteluväli: {salaryRange}</span>
            </div>
            <div>
                <span>Ilmoituksia: {count} kpl</span>
            </div>
        </div>
    ) : 
    null ;
}

export default SalarySummary;