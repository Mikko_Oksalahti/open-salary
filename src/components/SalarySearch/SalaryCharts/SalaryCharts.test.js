import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import 'jest-canvas-mock';

import SalaryCharts from "./SalaryCharts";

Enzyme.configure({ adapter: new Adapter() })

jest.mock('react-chartjs-2', () => ({
    Bar: (props) => <p {...props}></p>,
    Scatter: (props) => <p {...props}></p>
  }));

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders correct chart data", () => {

  // empty data
  act(() => {
    render(<SalaryCharts data={[]}/>, container);
  });
  expect(container.textContent).toBe("JakaumaTyökokemusSukupuoli");

  // realistic data

  const realData1 = JSON.parse('[{"experience":5,"gender":"male","occupation":"sofdev","province":"pohjoispohjanmaa","salary":3827,"timestamp":1616395813124},{"experience":2,"gender":"other","occupation":"sofdev","province":"pirkanmaa","salary":2715,"timestamp":1616150782604},{"experience":3,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":2869,"timestamp":1616154041167},{"experience":8,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":5400,"timestamp":1616333164298},{"experience":15,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":5500,"timestamp":1615899901445},{"experience":24,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":6000,"timestamp":1616582783300},{"experience":7,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":5250,"timestamp":1615900170576},{"experience":5,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":4400,"timestamp":1616237184504},{"experience":4,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":3500,"timestamp":1615915823220},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":2869,"timestamp":1616150989195},{"experience":3,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3177,"timestamp":1616482595874},{"experience":2,"gender":"male","occupation":"sofdev","province":"varsinaisuomi","salary":4170,"timestamp":1616150447710},{"experience":9,"gender":"female","occupation":"sofdev","province":"uusimaa","salary":5800,"timestamp":1615900108826},{"experience":7,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":4640,"timestamp":1616230793357},{"experience":9,"gender":"male","occupation":"sofdev","province":"pohjoispohjanmaa","salary":4050,"timestamp":1616156629531},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3100,"timestamp":1616151604624},{"experience":5,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3800,"timestamp":1616151521811},{"experience":1,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":2802,"timestamp":1616162733416},{"experience":20,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":5060,"timestamp":1616631832042},{"experience":3,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":4000,"timestamp":1616154315525},{"experience":2,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":3500,"timestamp":1615900133370},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3800,"timestamp":1616150414647},{"experience":8,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":4300,"timestamp":1615985274083},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3000,"timestamp":1616154124429}]');

  const wrapper = mount(<SalaryCharts data={realData1} />);

  const scas = wrapper.find('Scatter');
  const sca = scas.props().data.datasets[0].data;
  const asJSON = JSON.stringify(sca);

  expect(asJSON).toBe("[{\"x\":5,\"y\":3827},{\"x\":2,\"y\":2715},{\"x\":3,\"y\":2869},{\"x\":8,\"y\":5400},{\"x\":15,\"y\":5500},{\"x\":24,\"y\":6000},{\"x\":7,\"y\":5250},{\"x\":5,\"y\":4400},{\"x\":4,\"y\":3500},{\"x\":2,\"y\":2869},{\"x\":3,\"y\":3177},{\"x\":2,\"y\":4170},{\"x\":9,\"y\":5800},{\"x\":7,\"y\":4640},{\"x\":9,\"y\":4050},{\"x\":2,\"y\":3100},{\"x\":5,\"y\":3800},{\"x\":1,\"y\":2802},{\"x\":20,\"y\":5060},{\"x\":3,\"y\":4000},{\"x\":2,\"y\":3500},{\"x\":2,\"y\":3800},{\"x\":8,\"y\":4300},{\"x\":2,\"y\":3000}]");

  const bars = wrapper.find('Bar');
  const asJSON2 = JSON.stringify(bars.getElements()[1]);

  expect(asJSON2).toContain("[5800,3913.5,2715,0]");

});