import React, { useState, useEffect } from 'react';
import { Bar, Scatter } from 'react-chartjs-2';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

import { getOccupationSalaryChartData, median, ISalaryData } from '../../../statistics/statFunctions';

interface ISalaryChartsProps {
    data : ISalaryData[]
}

interface IGenderStats {
    femaleMedian : number,
    femaleCount : number,
    maleMedian : number,
    maleCount : number,
    otherMedian : number,
    otherCount : number,
    naMedian : number,
    naCount : number,
}

const SalaryCharts = (props : ISalaryChartsProps) => {

    const [ chartData, setChartData ] = useState({});

    useEffect( () => {
        setChartData(getOccupationSalaryChartData(props.data));
    }, [props.data]);

    let salaryExpData = [];
    if(props.data) {
        const sals : string[] = Object.keys(props.data);
        for(const entry in sals) {
            const e : ISalaryData = props.data[entry];
            if(e.experience >= 0 && e.experience < 60) {
                salaryExpData.push({x: e.experience, y: e.salary});
            }
        }
    }

    let scatterData = {
        datasets: [
            {
                label: 'Palkka €/kk & työkokemus vuosina',
                pointBackgroundColor: 'rgb(0, 50, 128)',
                data: salaryExpData
            }
        ]
    };
    
    const generateGenderStats = (data : ISalaryData[]) => {
        let ret : IGenderStats = {
            femaleMedian : 0,
            femaleCount : 0,
            maleMedian : 0,
            maleCount : 0,
            otherMedian : 0,
            otherCount : 0,
            naMedian : 0,
            naCount : 0
        };
        let femaleSals : number[] = [];
        let maleSals : number[] = [];
        let otherSals : number[] = [];
        let naSals : number[] = [];
        data.map(d => {
            if(d.gender === 'female') {
                femaleSals.push(d.salary);
            } else if(d.gender === 'male') {
                maleSals.push(d.salary);
            } else if(d.gender === 'other') {
                otherSals.push(d.salary);
            } else if(d.gender === 'none') {
                naSals.push(d.salary);
            }
            return d;
        });
        const comparator = (a : number, b : number) => a > b ? 1 : -1;
        ret.femaleMedian = femaleSals.length > 0 ? median(femaleSals, comparator) : 0;
        ret.femaleCount = femaleSals.length;
        ret.maleMedian = maleSals.length > 0 ? median(maleSals, comparator) : 0;
        ret.maleCount = maleSals.length;
        ret.otherMedian = otherSals.length > 0 ? median(otherSals, comparator) : 0;
        ret.otherCount = otherSals.length;
        ret.naMedian = naSals.length > 0 ? median(naSals, comparator) : 0;
        ret.naCount = naSals.length;
    
        return ret;
    }
    
    let genderData = {};
    if(props.data) {
        const statData = generateGenderStats(props.data);
        genderData = {
            labels: [
                'Nainen (' + statData.femaleCount + ')', 
                'Mies (' + statData.maleCount + ')', 
                'Muu (' + statData.otherCount + ')', 
                'Ei ilmoitettu (' + statData.naCount + ')'], 
            datasets: [
                {
                    label: "Keskipalkka sukupuolittain", 
                    backgroundColor: "rgb(1, 133, 194)", 
                    data: [statData.femaleMedian, statData.maleMedian, statData.otherMedian, statData.naMedian]
                }
            ]
        }
    }

    return (
        <Tabs
            defaultActiveKey="distribution" 
            id="uncontrolled-tab-example">
            <Tab eventKey="distribution" title="Jakauma">
                <div>
                    <Bar 
                        options={{scales: {yAxes: [{ticks: {beginAtZero: true, callback: (value : any, index : any, values : any) => {return value+'%';}}}]}}} 
                        data={chartData}>
                    </Bar>
                </div>
            </Tab>
            <Tab eventKey="salAndExp" title="Työkokemus">
                <div>
                    <Scatter 
                        options={{scales: {xAxes: [{type: 'linear', position: 'bottom', ticks: {callback: (value : any, index : any, values : any) => {return value + 'v';}}}], yAxes: [{ticks: {callback: (value : any, index : any, values : any) => {return value+'€';}}}]}}} 
                        data={scatterData}>
                    </Scatter>
                </div>
            </Tab>
            <Tab eventKey="genders" title="Sukupuoli">
                <div>
                    <Bar 
                        options={{scales: {yAxes: [{ticks: {beginAtZero: true, callback: (value : any, index : any, values : any) => {return value+'€';}}}]}}} 
                        data={genderData}>
                    </Bar>
                </div>
            </Tab>
        </Tabs>
    );
}

export default SalaryCharts;