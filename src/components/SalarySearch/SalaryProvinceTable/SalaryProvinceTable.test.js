import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { useSelector } from 'react-redux';

import SalarySProvinceTable from "./SalaryProvinceTable";

let container = null;

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useSelector: jest.fn()
}));

const mockAppState = {
    root: {
      provinces: {
      select: 'Valitse maakunta...',
      ahvenanmaa:'Ahvenanmaa', 
      etelakarjala:'Etelä-Karjala', 
      etelapohjanmaa:'Etelä-Pohjanmaa', 
      etelasavo:'Etelä-Savo', 
      kainuu:'Kainuu', 
      kantahame:'Kanta-Häme', 
      keskipohjanmaa:'Keski-Pohjanmaa', 
      keskisuomi:'Keski-Suomi', 
      kymenlaakso:'Kymenlaakso', 
      lappi:'Lappi', 
      pirkanmaa:'Pirkanmaa', 
      pohjanmaa:'Pohjanmaa', 
      pohjoiskarjala:'Pohjois-Karjala', 
      pohjoispohjanmaa:'Pohjois-Pohjanmaa', 
      pohjoissavo:'Pohjois-Savo', 
      paijathame:'Päijät-Häme', 
      satakunta:'Satakunta', 
      uusimaa:'Uusimaa', 
      varsinaisuomi:'Varsinais-Suomi'}
    }
};

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
  useSelector.mockImplementation(callback => {
    return callback(mockAppState);
  });
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders correct province table", () => {

  // empty data
  act(() => {
    render(<SalarySProvinceTable data={[]}/>, container);
  });
  expect(container.textContent).toBe("MaakuntaKeskipalkka / kkIlmoituksia");

  // realistic data

  const realData1 = JSON.parse('[{"experience":5,"gender":"male","occupation":"sofdev","province":"pohjoispohjanmaa","salary":3827,"timestamp":1616395813124},{"experience":2,"gender":"other","occupation":"sofdev","province":"pirkanmaa","salary":2715,"timestamp":1616150782604},{"experience":3,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":2869,"timestamp":1616154041167},{"experience":8,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":5400,"timestamp":1616333164298},{"experience":15,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":5500,"timestamp":1615899901445},{"experience":24,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":6000,"timestamp":1616582783300},{"experience":7,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":5250,"timestamp":1615900170576},{"experience":5,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":4400,"timestamp":1616237184504},{"experience":4,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":3500,"timestamp":1615915823220},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":2869,"timestamp":1616150989195},{"experience":3,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3177,"timestamp":1616482595874},{"experience":2,"gender":"male","occupation":"sofdev","province":"varsinaisuomi","salary":4170,"timestamp":1616150447710},{"experience":9,"gender":"female","occupation":"sofdev","province":"uusimaa","salary":5800,"timestamp":1615900108826},{"experience":7,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":4640,"timestamp":1616230793357},{"experience":9,"gender":"male","occupation":"sofdev","province":"pohjoispohjanmaa","salary":4050,"timestamp":1616156629531},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3100,"timestamp":1616151604624},{"experience":5,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3800,"timestamp":1616151521811},{"experience":1,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":2802,"timestamp":1616162733416},{"experience":20,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":5060,"timestamp":1616631832042},{"experience":3,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":4000,"timestamp":1616154315525},{"experience":2,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":3500,"timestamp":1615900133370},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3800,"timestamp":1616150414647},{"experience":8,"gender":"male","occupation":"sofdev","province":"uusimaa","salary":4300,"timestamp":1615985274083},{"experience":2,"gender":"male","occupation":"sofdev","province":"pirkanmaa","salary":3000,"timestamp":1616154124429}]');

  act(() => {
    render(<SalarySProvinceTable data={realData1} />, container);
  });
  expect(container.textContent).toContain("Pirkanmaa3488.5 €14");
  expect(container.textContent).toContain("Uusimaa5250 €7");
  expect(container.textContent).toContain("-3938.5 €2");

  const realData2 = JSON.parse('[{"experience":2,"gender":"male","occupation":"proman","province":"uusimaa","salary":5000,"timestamp":1615920702961},{"experience":2,"gender":"male","occupation":"proman","province":"uusimaa","salary":4160,"timestamp":1615962700199},{"experience":6,"gender":"male","occupation":"proman","province":"pirkanmaa","salary":4600,"timestamp":1616157306158}]');

  act(() => {
    render(<SalarySProvinceTable data={realData2} />, container);
  });
  expect(container.textContent).toContain("-4600 €1");
  expect(container.textContent).toContain("-4580 €2");

});