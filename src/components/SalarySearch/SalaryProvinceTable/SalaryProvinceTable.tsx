import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { getOccupationSalaryProvinceTable, IProvinceTable, ISalaryData } from '../../../statistics/statFunctions';
import { RootState } from '../../../store/store';
import { ILooseObject } from '../../../store/types';

import classes from './SalaryProvinceTable.module.css';

interface ISalaryProvinceTableProps {
    data : ISalaryData[]
}

const SalaryProvinceTable = (props : ISalaryProvinceTableProps) => {

    const provinces : ILooseObject = useSelector<RootState, ILooseObject>(state => state.root.provinces);

    const [ provinceTableData, setProvinceTableData ] = useState<IProvinceTable[]>([]);

    useEffect(() => {
        setProvinceTableData(getOccupationSalaryProvinceTable(props.data, provinces));
    }, [props.data, provinces]);

    return (
        <div className={classes.ProvinceTableDiv}>
            <table className={classes.ProvinceTable}>
                <tbody>
                    <tr className={classes.ProvinceTable_HeaderRow}><td>Maakunta</td><td>Keskipalkka / kk</td><td>Ilmoituksia</td></tr>
                    {provinceTableData.map(el => {
                        return (
                            <tr key={el.key}>
                                <td>{el.name}</td><td>{el.median} &euro;</td><td>{el.count}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalaryProvinceTable;