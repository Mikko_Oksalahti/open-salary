import QuestionIcon from '../SVGIcons/QuestionIcon';

import classes from './QuestionButton.module.css';

const QuestionButton = (props : any) => {
    return (
        <div {...props} className={classes.QuestionButton}>
            <QuestionIcon />
        </div>
    );
}

export default QuestionButton;