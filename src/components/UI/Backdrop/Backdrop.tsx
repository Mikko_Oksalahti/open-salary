import React from 'react';

import classes from './Backdrop.module.css';

interface IBackdropProps {
    dark? : boolean,
    onClick? : () => void,
    children? : React.ReactNode
};

const Backdrop = ( props : IBackdropProps ) => {
    let cls : string[] = [classes.Backdrop];

    if(props.dark) {
        cls.push(classes.Dark);
    } else {
        cls.push(classes.Light);
    }

    return (
        <div {...props} className={cls.join(' ')}>
            {props.children}
        </div>
    );
}

export default Backdrop;