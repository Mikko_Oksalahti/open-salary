import { NavLink } from 'react-router-dom';
import HomeIcon from '../SVGIcons/HomeIcon';

import classes from './HomeButton.module.css';

const HomeButton = () => {
    return (
        <NavLink to='/' className={classes.HomeButton}>
            <HomeIcon />
        </NavLink>
    );
}

export default HomeButton;