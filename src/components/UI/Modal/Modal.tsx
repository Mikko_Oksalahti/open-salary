import React from 'react';
import CloseIcon from '../SVGIcons/CloseIcon';

import classes from './Modal.module.css';

interface IModalProps {
    cancelModal : () => void,
    children : React.ReactNode
};

const Modal = ( props : IModalProps ) => {
    return (
        <div className={classes.Modal}>
            <div onClick={props.cancelModal} className={classes.CloseIcon}>
                <CloseIcon 
                    gray='true' 
                    hoverstyle={classes.HoverStyle}/>
            </div>
            {props.children}
        </div>
    );
}

export default Modal;