import React from 'react';

import classes from './PageBlock.module.css';

interface IPageBlockProps {
    background: string,
    centered?: boolean,
    children: React.ReactNode
}

const PageBlock = (props : IPageBlockProps) => {

    let styles : string[] = [classes.PageBlock];
    if(props.background === 'primary') {
        styles.push(classes.BackgroundPrimary);
    } else if(props.background === 'primary-dark') {
        styles.push(classes.BackgroundPrimaryDark);
    } else if(props.background === 'primary-light') {
        styles.push(classes.BackgroundPrimaryLight);
    }
    if(props.centered) {
        styles.push('centered');
    }

    return (
        <div className={styles.join(' ')}>
            {props.children}
        </div>
    );
}

export default PageBlock;