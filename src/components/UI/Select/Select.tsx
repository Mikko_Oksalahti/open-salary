import React from 'react';

import classes from './Select.module.css';

interface ISelectProps {
    value? : string,
    onChange? : any,
    id? : string,
    style? : any,
    children : React.ReactNode
};

const Select = ( props : ISelectProps ) => {
    return (
        <select className={classes.Select} {...props}>
            {props.children}
        </select>
    )
}

export default Select;