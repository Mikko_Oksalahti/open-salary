import React from 'react';
import InfoIcon from '../SVGIcons/InfoIcon';

import classes from './InfoIconWithHoverPopup.module.css';

interface IInfoIconWithHoverPopupProps {
    children: React.ReactNode
};

const InfoIconWithHoverPopup = ( props : IInfoIconWithHoverPopupProps ) => {
    return (
        <div className={classes.InfoIconWithHoverPopup}>
            <InfoIcon className={classes.InfoIcon} />
            <div className={classes.Popup}>
                {props.children}
            </div>
        </div>
    );
}

export default InfoIconWithHoverPopup;