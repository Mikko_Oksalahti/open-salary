import React from 'react';

import classes from './Button.module.css';

interface IButtonProps {
    secondary? : boolean,
    widefixed? : boolean,
    disabled? : boolean,
    children : React.ReactNode,
    onClick? : () => void
};

const Button = ( props : IButtonProps ) => {
    let cls : string[] = [classes.Button];
    if(props.secondary) {
        cls.push(classes.SecondaryButton);
    }
    if(props.widefixed) {
        cls.push(classes.FixedWide);
    }
    if(props.disabled) {
        cls.push(classes.Disabled);
    }
    return (
        <div {...props} className={cls.join(' ')}>
            {props.children}
        </div>
    );
}

export default Button;