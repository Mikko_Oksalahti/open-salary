import classes from './Input.module.css';

interface IInputProps {
    small? : boolean,
    required? : boolean,
    maxLength? : number,
    type? : string,
    value? : string,
    onChange? : any,
    autoFocus? : boolean,
    placeholder? : string,
    id? : string,
    children? : React.ReactNode
};

const Input = ( props : IInputProps ) => {

    let styles = [classes.Input];
    if(props.small) {
        styles.push(classes.InputSmallWidth);
    } else {
        styles.push(classes.InputNormalWidth);
    }

    return (
        <input 
            className={styles.join(' ')}
            {...props}>
        </input>
    );
}

export default Input;