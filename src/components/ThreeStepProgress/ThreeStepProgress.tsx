import classes from './ThreeStepProgress.module.css';

interface IThreeStepProgressProps {
    phase : number
};

const ThreeStepProgress = ( props : IThreeStepProgressProps ) => {

    let step1Styles : string[] = [classes.Step1, classes.Step];
    let step2Styles : string[] = [classes.Step2, classes.Step];
    let step3Styles : string[] = [classes.Step3, classes.Step];

    step1Styles.push(props.phase === 1 ? classes.StepActive : (props.phase > 1 ? classes.StepDone : classes.StepComing));
    step2Styles.push(props.phase === 2 ? classes.StepActive : (props.phase > 2 ? classes.StepDone : classes.StepComing));
    step3Styles.push(props.phase === 3 ? classes.StepActive : (props.phase > 3 ? classes.StepDone : classes.StepComing));

    return (
        <div className={classes.ThreeStepProgress}>
            <div className={classes.ConnectorLine} />
            <div className={step1Styles.join(' ')}>1</div>
            <div className={step2Styles.join(' ')}>2</div>
            <div className={step3Styles.join(' ')}>3</div>
        </div>
    );
}

export default ThreeStepProgress;