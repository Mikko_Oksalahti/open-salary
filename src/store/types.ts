export interface ILooseObject {
    [key: string]: any
}

interface IUIState {
    fetching: boolean,
    noData: boolean
}

interface IOverallUIState {
    fetching: boolean
}

export interface IState {
    occupationList: ILooseObject,
    salaryDataCache: ILooseObject,
    salaryDataUIState: IUIState,
    overallStatsCache: ILooseObject | null,
    overallStatsUIState: IOverallUIState,
    httpError: string | null,
    provinces: ILooseObject,
    genders: ILooseObject
}