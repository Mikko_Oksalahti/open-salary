import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit'
import axios, { FIREBASE_API_URL, AWS_API_URL } from '../../axios';

import { IState, ILooseObject } from '../types';

// TODO: Divide state into smaller logical slices

const initialState : IState = {
    occupationList: {},
    salaryDataCache: {},
    salaryDataUIState: {
        fetching: false,
        noData: false
    },
    overallStatsCache: null,
    overallStatsUIState: {
        fetching: false
    },

    httpError: null,
    provinces: {
        select: 'Valitse maakunta...',
        ahvenanmaa:'Ahvenanmaa', 
        etelakarjala:'Etelä-Karjala', 
        etelapohjanmaa:'Etelä-Pohjanmaa', 
        etelasavo:'Etelä-Savo', 
        kainuu:'Kainuu', 
        kantahame:'Kanta-Häme', 
        keskipohjanmaa:'Keski-Pohjanmaa', 
        keskisuomi:'Keski-Suomi', 
        kymenlaakso:'Kymenlaakso', 
        lappi:'Lappi', 
        pirkanmaa:'Pirkanmaa', 
        pohjanmaa:'Pohjanmaa', 
        pohjoiskarjala:'Pohjois-Karjala', 
        pohjoispohjanmaa:'Pohjois-Pohjanmaa', 
        pohjoissavo:'Pohjois-Savo', 
        paijathame:'Päijät-Häme', 
        satakunta:'Satakunta', 
        uusimaa:'Uusimaa', 
        varsinaisuomi:'Varsinais-Suomi'},
    genders: {
        select: 'Valitse sukupuoli...',
        female:'Nainen', 
        male:'Mies', 
        other:'Muu', 
        none:'En ilmoita'}
}

export const fetchOccupations = createAsyncThunk('occupations/fetch', async () => {
    const response = await axios.get(FIREBASE_API_URL + '/occupation-list.json');
    return response.data;
});

export const fetchOverallStats = createAsyncThunk('overallstats/fetch', async () => {
    const response = await axios.get(AWS_API_URL + '?getstats');
    return response.data.data;
});

export const fetchSalaryDataForOccupation =  createAsyncThunk('salarydata/fetch', async (occupation : string) => {
    const response = await axios.get(FIREBASE_API_URL + '/salaries.json?orderBy="occupation"&equalTo="' + occupation + '"')
    return {occupation: occupation, data: response.data};
});

const rootSlice = createSlice({
    name: 'root',
    initialState,
    reducers: {
        setHttpError(state, action : PayloadAction<string> ) {
            state.httpError = action.payload;
            state.overallStatsUIState.fetching = false;
        },
        clearHttpError(state) {
            state.httpError = null;
        },
        addNewOccupation(state, action : PayloadAction<string>) {
            state.occupationList = {...state.occupationList, newocc: action.payload};
        }
    },
    extraReducers: builder => {
        builder
          .addCase(fetchOccupations.fulfilled, (state, action : PayloadAction<ILooseObject>) => {

            const response = action.payload;
            // sort the object's content by occupation name/value
            let orderedOccupationList : ILooseObject = {};
            const ord : string[] = Object.keys(response);
            ord.sort((a, b) => (response[a] > response[b] ? 1 : -1));
            for(const o of ord) {
                orderedOccupationList[o] = response[o];
            }
            state.occupationList = orderedOccupationList;

          })
          .addCase(fetchOverallStats.pending, (state) => {
            state.overallStatsUIState.fetching = true;
          })
          .addCase(fetchOverallStats.fulfilled, (state, action : PayloadAction<ILooseObject>) => {
            state.overallStatsCache = action.payload;
            state.overallStatsUIState.fetching = false;
          })
          .addCase(fetchSalaryDataForOccupation.pending, (state) => {
            state.salaryDataUIState.fetching = true;
          })
          .addCase(fetchSalaryDataForOccupation.fulfilled, (state, action : PayloadAction<ILooseObject>) => {

            const data = Object.keys(action.payload.data).map(el => action.payload.data[el]);
            if(data.length > 0) {
                state.salaryDataCache[action.payload.occupation] = data;
                state.salaryDataUIState.noData = false;
            } else {
                state.salaryDataUIState.noData = true;
            }
            state.salaryDataUIState.fetching = false;

          })
      }
  }
);

export const { setHttpError, 
               clearHttpError,
               addNewOccupation
             } = rootSlice.actions;

export default rootSlice.reducer;