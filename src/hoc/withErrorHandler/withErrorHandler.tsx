import React, { useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../../store/hooks';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import Modal from '../../components/UI/Modal/Modal';
import Button from '../../components/UI/Button/Button';
import { setHttpError, clearHttpError } from '../../store/reducers/reducer';
import { AxiosInstance } from 'axios';

const withErrorHandling = (WrappedComponent : any, axios : AxiosInstance) => {
    const WithErrorHandling = (props : any) => {

        const error = useAppSelector(state => state.root.httpError);
        const dispatch = useAppDispatch();

        const reqInterceptor = axios.interceptors.request.use(request => {
            dispatch(clearHttpError());
            return request;
        });
        
        const resInterceptor = axios.interceptors.response.use(res => res, error => {
            dispatch(setHttpError(error.message));
        });
        
        useEffect(() => {
            return () => {
                if(reqInterceptor) {
                    axios.interceptors.request.eject(reqInterceptor);
                }
                if(resInterceptor) {
                    axios.interceptors.request.eject(resInterceptor);
                }
            }
        }, [reqInterceptor, resInterceptor]);

        const errorConfirmedHandler = () => {
            dispatch(clearHttpError());
        }

        const errorModal = error ? (
            <React.Fragment>
                <Backdrop onClick={errorConfirmedHandler} />
                <Modal cancelModal={errorConfirmedHandler}> 
                    <div className='centered fancy-title'>Hmm...</div> 
                    <div className='t-margin-small'>Yhteys palvelimeen ei tunnu toimivan. Yritä pian uudelleen.</div>
                    <div className='t-margin-small' style={{color:'#888'}}><small>({error !== null ? error : null})</small></div>
                    <div className='centered t-margin-medium'><Button onClick={errorConfirmedHandler}>OK</Button></div>
                </Modal>
            </React.Fragment>
        ) : null;

        return (
            <React.Fragment>
                {errorModal}
                <WrappedComponent {...props} />           
            </React.Fragment>
        );
    }
    return WithErrorHandling;
}

export default withErrorHandling;