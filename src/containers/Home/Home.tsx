import React from 'react';
import { NavLink } from 'react-router-dom';
import Button from '../../components/UI/Button/Button';
import PageBlock from '../../components/UI/PageBlock/PageBlock';
import SendIcon from '../../components/UI/SVGIcons/SendIcon';
import SearchIcon from '../../components/UI/SVGIcons/SearchIcon';
import StatsIcon from '../../components/UI/SVGIcons/StatsIcon';

import classes from './Home.module.css';

const Home = () => {
    return (
        <div className={classes.Home}>
            <PageBlock background='primary-dark'>
                <div className={classes.HomeLogo}>
                    <span>Palkat esiin</span><span className={classes.SmallSuperscript}>(beta)</span>
                </div>
            </PageBlock>

            <PageBlock background='primary'>
                <div className={[classes.row, classes.QuoteLike, classes.AppearQuick].join(' ')}>
                    <div className={[classes.col1of3, classes.col1].join(' ')}>
                        Oletko hakemassa uutta työpaikkaa etkä tiedä miten paljon voisit pyytää palkkaa? Mietitkö saatko yhtä hyvää palkkaa kuin muut ammattikuntasi edustajat?
                    </div>
                    <div className={[classes.col1of3, classes.col2].join(' ')}>
                        Palkkatiedot ovat Suomessa eräänlainen tabu ja valtaosa ei tiedä mikä heidän ammattinsa todellinen palkkataso on.
                    </div>
                    <div className={[classes.col1of3, classes.col3].join(' ')}>
                        Tämä palvelu haluaa tuoda palkkatiedot näkyville, ettei kenenkään tarvitsisi arvata millaista palkkaa heidän kuuluisi saada tietystä työstä.
                    </div>
                </div>
            </PageBlock>

            <PageBlock background='primary-light' centered>
                <div className={classes.AppearMedium}>
                    <NavLink style={{textDecoration:'none'}} to='/submit-salary'>
                        <Button widefixed>
                            <SendIcon className={classes.ButtonIcon} />
                            <div>Ilmoita palkkasi nimettömänä</div>
                        </Button>
                    </NavLink>

                    <NavLink style={{textDecoration:'none'}} to='/salary-search'>
                        <Button widefixed>
                            <SearchIcon className={classes.ButtonIcon} />
                            <div>Hae tietyn ammatin palkkatietoja</div>
                        </Button>
                    </NavLink>

                    <NavLink style={{textDecoration:'none'}} to='/statistics'>
                        <Button widefixed>
                            <StatsIcon className={classes.ButtonIcon} />
                            <div>Tarkastele yleisiä palkkatilastoja</div>
                        </Button>
                    </NavLink>
                </div>
            </PageBlock>

            <div className={classes.Footer}>
                <div><span>Tämä on beta-versio.</span><span className={classes.ExtraInfo}> Palvelu sisältää tällä hetkellä vain IT- ja ohjelmistoalan ammattinimikkeitä</span></div>
                <div className={classes.TinyVersionInfo}>v: {process.env.REACT_APP_VERSION}</div>
            </div>
        </div>
    );
}

export default Home;