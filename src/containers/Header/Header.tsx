import AppLogo from '../../components/AppLogo/AppLogo';
import HomeButton from '../../components/UI/HomeButton/HomeButton';

import classes from './Header.module.css';

const Header = () => {
    return (
        <div className={classes.Header}>
            <HomeButton />
            <AppLogo />
        </div>
    );
}

export default Header;