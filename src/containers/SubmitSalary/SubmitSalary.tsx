import React, { useState, useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../../store/hooks';
import { NavLink } from 'react-router-dom';
import Header from '../Header/Header';
import ThreeStepProgress from '../../components/ThreeStepProgress/ThreeStepProgress';
import Button from '../../components/UI/Button/Button';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import Modal from '../../components/UI/Modal/Modal';
import AddNewOccupation from '../../components/SubmitSalary/AddNewOccupation/AddNewOccupation';
import SubmitStepInstructions from '../../components/SubmitSalary/SubmitStepInstructions/SubmitStepInstructions';
import SubmitStepForm from '../../components/SubmitSalary/SubmitStepForm/SubmitStepForm';
import SubmitStepSummary from '../../components/SubmitSalary/SubmitStepSummary/SubmitStepSummary';
import SubmitStepCongratulations from '../../components/SubmitSalary/SubmitStepCongratulations/SubmitStepCongratulations';
import ChallengeModal from '../../components/SubmitSalary/ChallengeModal/ChallengeModal';
import { addNewOccupation } from '../../store/reducers/reducer';

import classes from './SubmitSalary.module.css';
import { ILooseObject } from '../../store/types';

export interface ISubmitData {
    salary : string,
    occupation: string,
    experience: string,
    gender: string,
    province: string
}

const SubmitSalary = () => {

    const occupationList : ILooseObject = useAppSelector(state => state.root.occupationList);
    const provinces : ILooseObject = useAppSelector(state => state.root.provinces);
    const genders : ILooseObject = useAppSelector(state => state.root.genders);

    const [ progress, setProgress ] = useState<number>(1);
    const [ salary, setSalary ] = useState<string>('');
    const [ occupation, setOccupation ] = useState<string>('select');
    const [ experience, setExperience ] = useState<string>('');
    const [ gender, setGender ] = useState<string>('select');
    const [ province, setProvince ] = useState<string>('select');
    const [ newOccupationModalOpen, setNewOccupationModalOpen ] = useState<boolean>(false);
    const [ occupations, setOccupations ] = useState<ILooseObject>({});
    const [ validationMessage, setValidationMessage ] = useState<string>('');
    const [ oneMissing, setOneMissing ] = useState<boolean>(false);
    const [ challengeModalOpen, setChallengeModalOpen] = useState<boolean>(false);

    const dispatch = useAppDispatch();

    useEffect(() => {
        const newOccupations : ILooseObject = {
            select: "Valitse ammattinimike...",
            ...occupationList,
            addoccupation: "+ Lisää uusi ammattinimike..."
        };
        setOccupations(newOccupations);
    }, [occupationList]);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [progress]);

    const continueProgress = () => {
        if(progress === 2) {
            if(!validateForm()) {
                return;
            }
        }
        if(progress === 3) {
            setChallengeModalOpen(true);
            return;
        }
        setProgress(prev => (prev < 3 ? prev + 1 : prev));
    }

    const goBack = () => {
        setProgress(prev => (prev - 1));
    }

    const occupationSelected = (occ : string) => {
        if(occ === 'addoccupation') {
            setNewOccupationModalOpen(true);
        } else {
            setOccupation(occ);
        }
    }

    const closeModals = () => {
        setNewOccupationModalOpen(false);
        setValidationMessage('');
        setChallengeModalOpen(false);
    }

    const addOccupationHandler = (newOccupationName : string) => {
        dispatch(addNewOccupation(newOccupationName));
        setOccupation('newocc');
        setNewOccupationModalOpen(false);
    }

    let content : React.ReactNode = null;
    let progressButtons : React.ReactNode = null;
    
    if(progress === 1) {
        content = <SubmitStepInstructions />;
    } else if(progress === 2) {
        content = (
            <SubmitStepForm 
                occupations={occupations}
                genders={genders}
                provinces={provinces}
                salary={salary}
                occupation={occupation}
                experience={experience}
                gender={gender}
                province={province}
                setSalary={setSalary}
                setExperience={setExperience}
                setGender={setGender}
                setProvince={setProvince}
                occupationSelected={occupationSelected}
            />
        );
    } else if(progress === 3) {
        content = (
            <SubmitStepSummary
                occupation={occupations[occupation]}
                salary={salary}
                experience={experience}
                gender={genders[gender]}
                province={provinces[province]}
            />
        );
    } else if(progress === 4) {
        content = <SubmitStepCongratulations/>
    }

    const continueButton : React.ReactNode = (
        <div className='on-right'>
            <Button onClick={continueProgress}><div>{progress < 3 ? 'Jatka' : 'Lähetä'}</div></Button>
        </div>
    )

    if(progress === 1) {
        progressButtons = (
            <div className='narrow-text-box centered t-margin-medium'>
                <div className='on-left'>
                    <NavLink to='/'>
                        <Button secondary><div>Kumoa</div></Button>
                    </NavLink>
                </div>
                {continueButton}
            </div>
        );
    } else {
        progressButtons = (
            <div className='narrow-text-box centered t-margin-medium'>
                <div className='on-left'>
                    <Button secondary onClick={goBack}><div>Takaisin</div></Button>
                </div>
                {continueButton}
            </div>
        );
    }

    const validateForm = () => {
        const missing : string[] = [];
        if((salary.length <= 0) || isNaN(parseInt(salary))) missing.push('palkka');
        if(occupation === 'select') missing.push('ammatti');
        if((experience.length <= 0) || isNaN(parseInt(experience))) missing.push('työkokemus');
        if(gender === 'select') missing.push('sukupuoli');
        if(province === 'select') missing.push('maakunta');
        let missingOnlyOne = false;
        if(missing.length > 0) {
            let msg = '';
            if(missing.length > 1) {
                const last = missing.pop();
                msg = missing.join(', ') + ' ja ' + last;
            } else {
                missingOnlyOne = true;
                msg = missing[0];
            }
            setOneMissing(missingOnlyOne);
            setValidationMessage(msg);
            return false;
        } else {
            setOneMissing(missingOnlyOne);
            return true;
        }
    }

    const validationModelOpen = validationMessage.length > 0;
    let validationContent = null;
    if(validationModelOpen) {
        validationContent = (
            <Modal cancelModal={closeModals}>
                <div className='centered fancy-title'>Täytä vielä {oneMissing ? 'tämä' : 'nämä'}</div> 
                <div className='t-margin-small centered'>{validationMessage}</div>
                <div className='centered t-margin-medium'><Button onClick={closeModals}><div>OK</div></Button></div>
            </Modal>
        );
    }

    const closeChallengeModal = () => {
        setChallengeModalOpen(false);
    }

    const salarySubmitted = () => {
        setProgress(4);
        closeChallengeModal();
    }

    let submitData : ISubmitData = {
        salary: '',
        occupation: '',
        experience: '',
        gender: '',
        province: ''
    };
    if(challengeModalOpen) {
        submitData = {
            salary: salary,
            occupation: occupation,
            experience: experience,
            gender: gender,
            province: province
        }
    }

    return (
        <div className={classes.SubmitSalary}>
            <Header />

            {(newOccupationModalOpen || validationModelOpen || challengeModalOpen) && <Backdrop onClick={closeModals}/>}
            {validationModelOpen && validationContent}
            {newOccupationModalOpen && <AddNewOccupation close={closeModals} addOccupation={addOccupationHandler}/>}
            {challengeModalOpen && <ChallengeModal occupations={occupations} submitData={submitData} close={closeChallengeModal} done={salarySubmitted}/>}

            <div className='centered vertical-gutter-big'>
                <ThreeStepProgress phase={progress} />
            </div>

            <div className='centered'>
                {content}
                {(progress < 4) && progressButtons}
            </div>
        </div>
    );
}

export default SubmitSalary;