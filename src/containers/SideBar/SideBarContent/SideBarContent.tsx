import React from 'react';
import CloseIcon from '../../../components/UI/SVGIcons/CloseIcon';

import classes from './SideBarContent.module.css';

interface ISideBarContentProps {
    open? : boolean,
    closeSideBar: () => void
}

const SideBarContent = (props : ISideBarContentProps) => {
    return (
        <div 
            style={{
                right: props.open ? '0rem' : '-60rem',
                boxShadow: '0 0 2rem rgba(0, 0, 0, .5)'
            }} className={classes.SideBarContent}>
            <div
                className={classes.CloseIcon}
                onClick={props.closeSideBar}>
                <CloseIcon />
            </div>
            <div className={classes.SideBarText}>
                <p className='fancy-title hilite-color'>Palkat esiin -palvelu</p>
                <p>Tämän palvelun tarkoituksena on kerätä palkkatietoja nimettömänä ja tuoda ne näkyviin kaikille, jotka ovat kiinnostuneita eri ammattien palkoista.</p>
                <p>Palkkatiedot ovat Suomessa eräänlainen tabu ja valtaosa ei tiedä mikä heidän ammattinsa todellinen palkkataso on.</p>
                <p>Palkat esiin -palvelu ei kerää mitään tietoja minkä perusteella palkkansa ilmoittaneita voisi tunnistaa. Palvelu ei myöskään käytä mitään evästeitä tai sijaintipalveluita.</p>
                <p>Palvelu on beta-vaiheessa ja sisältää nyt aluksi vain IT- ja ohjelmistoalan ammattinimikkeitä. HUOM: Voit lisätä uuden ammattinimikkeen kun ilmoitat palkkasi.</p>
                <p className='t-margin-small'><strong>Toteutuksesta</strong></p>
                <p>Koodasin tämän palvelun koska halusin harjoitella React:in käyttöä. Sivusto on toteutetttu ns. single page app:ina (SPA) käyttäen seuraavia kirjastoja:</p>
                <p>
                    <code>
                        react,
                        react-router,
                        react-router-dom,
                        react-redux,
                        redux,
                        redux-thunk,
                        react-chartjs-2,
                        react-bootstrap,
                        bootstrap,
                        chart.js,
                        axios
                    </code>
                </p>
                <p>Backend tietokantana toimii Googlen Firebase Realtime Database. Olen arvioinut että palkkatietoja voidaan ilmoittaa n. 10 miljoonaa kappaletta ennen kuin backend palvelu muuttuu maksulliseksi. Palvelu käyttää myös Amazonin AWS Lambda funktioita palkkatietojen lähettämisen ja kokonaistilastojen hakemisen yhteydessä.</p>
                <p>Palvelun lähdekoodi on avoimesti nähtävissä Bitbucket.org:ssa:</p>
                <p>
                    <code>
                        <a 
                            target='_blank'
                            rel="noreferrer"
                            className={classes.Link} 
                            href='https://bitbucket.org/Mikko_Oksalahti/open-salary/'>
                                https://bitbucket.org/Mikko_Oksalahti/open-salary/
                        </a>
                    </code>
                </p>
                <p>Jos haluat antaa palautetta tai sinulla on ideoita/ehdotuksia, voit laittaa viestiä osoitteeseen palkat.esiin@gmail.com.</p>
                <p>&copy; 2021 Mikko Oksalahti</p>
            </div>
        </div>
    );
}

export default SideBarContent;