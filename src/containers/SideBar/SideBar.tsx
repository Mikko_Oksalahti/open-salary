import React, { useState } from 'react';
import SideBarContent from './SideBarContent/SideBarContent';
import QuestionButton from '../../components/UI/QuestionButton/QuestionButton';
import Backdrop from '../../components/UI/Backdrop/Backdrop';

const SideBar = () => {

    const [ barOpen, setBarOpen ] = useState<boolean>(false);

    const toggleBar = () => {
        setBarOpen(prev => !prev);
    }

    return (
        <React.Fragment>
            <QuestionButton onClick={toggleBar}/>
            {barOpen && <Backdrop dark onClick={toggleBar}/>}
            <SideBarContent open={barOpen} closeSideBar={toggleBar} />
        </React.Fragment>
    );
}

export default SideBar;