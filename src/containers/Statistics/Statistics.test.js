import React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import 'jest-canvas-mock';

import Statistics from "./Statistics";

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useSelector: jest.fn(),
    useDispatch: jest.fn()
}));

const testData = '{"count":62,"salaryAndExperience":"5500 15,5800 9,3500 2,5250 7,3700 3,3600 3,3500 4,5000 2,6100 20,6500 3,5137 11,4160 2,2775 1,4550 8,9000 3,3300 0,4000 2,4300 8,6258 2,4800 8,6150 8,3720 2,5500 5,3800 2,4170 2,2715 2,4876 6,2869 2,3800 5,3100 2,4218 1,2869 3,3000 2,4000 3,4100 10,4050 9,7700 22,4600 6,2802 1,3849 2,4640 7,4400 5,3745 2,5400 8,3200 5,3827 5,3700 4,3177 3,2450 2,5620 2,6000 24,5060 20,6400 18,2500 10,4200 3,6500 1,3700 1,5300 3,5000 5,2800 0,8700 5,4666 5","femaleMedian":3950,"femaleCount":10,"maleMedian":4218,"maleCount":49,"otherMedian":2715,"otherCount":1,"naMedian":4125,"naCount":2,"expires":1617193033245}';

const mockAppState = {
    root: {
      overallStatsCache: JSON.parse(testData),
      overallStatsUIState: {
          fetching: false
      }
   }
};

Enzyme.configure({ adapter: new Adapter() })

jest.mock('react-chartjs-2', () => ({
    Bar: (props) => <p {...props}></p>,
    Scatter: (props) => <p {...props}></p>
  }));

let container = null;
beforeEach(() => {
  jest.clearAllMocks();
  // setup mockers
  useSelector.mockImplementation(callback => {
    return callback(mockAppState);
  });
  useDispatch.mockImplementation(callback => {
    return null;
  });
});

it("renders correct general statistics data", () => {

  // realistic data
  const wrapper = mount(<Router><Statistics /></Router>);

  const d = wrapper.find('div');
  const dAsJSON = JSON.stringify(d.getElements()[2]);
  expect(dAsJSON).toContain("Tietoja on vielä niin vähän (62 kpl) että mitään tilastollisia johtopäätöksiä ei voi tehdä.");
  expect(dAsJSON).toContain('\"className\":\"InfoNotEnoughData\"');

  const scas = wrapper.find('Scatter');
  const sca = scas.props().data.datasets[0].data;
  const scaAsJSON = JSON.stringify(sca);

  expect(scaAsJSON).toBe("[{\"x\":15,\"y\":5500},{\"x\":9,\"y\":5800},{\"x\":2,\"y\":3500},{\"x\":7,\"y\":5250},{\"x\":3,\"y\":3700},{\"x\":3,\"y\":3600},{\"x\":4,\"y\":3500},{\"x\":2,\"y\":5000},{\"x\":20,\"y\":6100},{\"x\":3,\"y\":6500},{\"x\":11,\"y\":5137},{\"x\":2,\"y\":4160},{\"x\":1,\"y\":2775},{\"x\":8,\"y\":4550},{\"x\":3,\"y\":9000},{\"x\":2,\"y\":4000},{\"x\":8,\"y\":4300},{\"x\":2,\"y\":6258},{\"x\":8,\"y\":4800},{\"x\":8,\"y\":6150},{\"x\":2,\"y\":3720},{\"x\":5,\"y\":5500},{\"x\":2,\"y\":3800},{\"x\":2,\"y\":4170},{\"x\":2,\"y\":2715},{\"x\":6,\"y\":4876},{\"x\":2,\"y\":2869},{\"x\":5,\"y\":3800},{\"x\":2,\"y\":3100},{\"x\":1,\"y\":4218},{\"x\":3,\"y\":2869},{\"x\":2,\"y\":3000},{\"x\":3,\"y\":4000},{\"x\":10,\"y\":4100},{\"x\":9,\"y\":4050},{\"x\":22,\"y\":7700},{\"x\":6,\"y\":4600},{\"x\":1,\"y\":2802},{\"x\":2,\"y\":3849},{\"x\":7,\"y\":4640},{\"x\":5,\"y\":4400},{\"x\":2,\"y\":3745},{\"x\":8,\"y\":5400},{\"x\":5,\"y\":3200},{\"x\":5,\"y\":3827},{\"x\":4,\"y\":3700},{\"x\":3,\"y\":3177},{\"x\":2,\"y\":2450},{\"x\":2,\"y\":5620},{\"x\":24,\"y\":6000},{\"x\":20,\"y\":5060},{\"x\":18,\"y\":6400},{\"x\":10,\"y\":2500},{\"x\":3,\"y\":4200},{\"x\":1,\"y\":6500},{\"x\":1,\"y\":3700},{\"x\":3,\"y\":5300},{\"x\":5,\"y\":5000},{\"x\":5,\"y\":8700},{\"x\":5,\"y\":4666}]");

  const bars = wrapper.find('Bar');
  const bar = bars.props().data.datasets[0].data;
  const barAsJSON = JSON.stringify(bar);

  expect(barAsJSON).toContain("[3950,4218,2715,4125]");

});