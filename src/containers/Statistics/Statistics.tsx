import React, { useCallback, useEffect } from 'react';
import { Bar, Scatter } from 'react-chartjs-2';
import { useAppSelector, useAppDispatch } from '../../store/hooks';
import Header from '../Header/Header';
import classes from './Statistics.module.css';
import { fetchOverallStats } from '../../store/reducers/reducer';
import { ILooseObject } from '../../store/types';

const Statistics = () => {

    const statData = useAppSelector(state => state.root.overallStatsCache);    
    const fetching = useAppSelector(state => state.root.overallStatsUIState.fetching);    

    const dispatch = useAppDispatch();
    const fetchOverallStatsFunc = useCallback(() => dispatch(fetchOverallStats()), [dispatch]);

    useEffect(() => {
        if(statData === null) {
            fetchOverallStatsFunc();
        }
    }, [statData, fetchOverallStatsFunc]);

    let infoText : string = 'Hmm. Tietoja ei saatu nyt haettua. Yritä hetken päästä uudelleen.';
    let infoTextClass : string = classes.InfoError;
    if(statData && statData.count) {
        if(statData.count < 500) {
            infoText = 'HUOM: Tietoja on vielä niin vähän (' + statData.count + ' kpl) että mitään tilastollisia johtopäätöksiä ei voi tehdä.';
            infoTextClass = classes.InfoNotEnoughData;
        } else if(statData.count < 1000) {
            infoText = 'HUOM: Tietoja on vielä alle 1000 (' + statData.count + ' kpl) joten tulokset ovat vasta suuntaa-antavia.';
            infoTextClass = classes.InfoNotQuiteEnoughData;
        } else {
            infoText = 'Tietoja on annettu ' + statData.count + ' kpl.';
            infoTextClass = classes.InfoOKData;
        }
    }

    let statisticsContent : React.ReactNode = null;
    if(statData) {

        let salaryExpData : ILooseObject[] = [];

        const entries : string[] = statData.salaryAndExperience.split(',');
        for(const entry of entries) {
            const coords : string[] = entry.split(' ');
            const experience : number = parseInt(coords[1]);
            const salary : number = parseInt(coords[0]);
            if(experience > 0 && experience < 60) {
                salaryExpData.push({x: experience, y: salary});
            }
        }

        let scatterData : ILooseObject = {datasets: [
            {
                label: 'Palkka €/kk & työkokemus vuosina',
                pointBackgroundColor: 'rgb(0, 50, 128)',
                data: salaryExpData
            }
        ]};
        
        const genderData : ILooseObject = {
            labels: [
                'Nainen (' + statData.femaleCount + ')', 
                'Mies (' + statData.maleCount + ')', 
                'Muu (' + statData.otherCount + ')', 
                'Ei ilmoitettu (' + statData.naCount + ')'], 
            datasets: [
                {
                    label: "Keskipalkka sukupuolittain", 
                    backgroundColor: "rgb(1, 133, 194)", 
                    data: [statData.femaleMedian, statData.maleMedian, statData.otherMedian, statData.naMedian]
                }
            ]
        }

        statisticsContent = (
            <React.Fragment>
                <div className={infoTextClass}>
                    {infoText}
                </div>
                {statData && (
                    <React.Fragment>
                        <div className={classes.WithAir}>
                            <Scatter 
                                options={{scales: {xAxes: [{type: 'linear', position: 'bottom', ticks: {callback: function(value : any, index : any, values : any) {return value + 'v';}}}], yAxes: [{ticks: {callback: (value : any, index : any, values : any) => {return value+'€';}}}]}}} 
                                data={scatterData}>
                            </Scatter>
                        </div>
                        <div className={classes.WithAir}>
                            <Bar 
                                options={{scales: {yAxes: [{ticks: {beginAtZero: true, callback: (value : any, index : any, values : any) => {return value+'€';}}}]}}} 
                                data={genderData}>
                            </Bar>
                        </div>
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <Header />
            <div className={['narrow-text-box', classes.Statistics, 'left-justified'].join(' ')}>
                {fetching ? 
                    (
                        <div className={[classes.Fetching, 'centered'].join(' ')}>Haetaan palkkatilastoja...</div> 
                    ) : ( 
                        statisticsContent
                    )}
            </div>
        </React.Fragment>
    );
}

export default Statistics;