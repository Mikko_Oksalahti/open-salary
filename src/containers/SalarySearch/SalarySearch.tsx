import React, { useState, useCallback } from 'react';
import { NavLink } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../store/hooks';
import Header from '../Header/Header';
import Select from '../../components/UI/Select/Select';
import Button from '../../components/UI/Button/Button';
import SalarySummary from '../../components/SalarySearch/SalarySummary/SalarySummary';
import SalaryCharts from '../../components/SalarySearch/SalaryCharts/SalaryCharts';
import SalaryProvinceTable from '../../components/SalarySearch/SalaryProvinceTable/SalaryProvinceTable';
import { ILooseObject } from '../../store/types';
import { fetchSalaryDataForOccupation } from '../../store/reducers/reducer';

import classes from './SalarySearch.module.css';

const SalarySearch = () => {

    const occupationList = useAppSelector(state => state.root.occupationList);
    const salaryDataCache = useAppSelector(state => state.root.salaryDataCache);
    const fetching = useAppSelector(state => state.root.salaryDataUIState.fetching);
    const noData = useAppSelector(state => state.root.salaryDataUIState.noData);

    const [ occupation, setOccupation ] = useState<string>('');

    const dispatch = useAppDispatch();
    const fetchSalaryData = useCallback((occupation) => dispatch(fetchSalaryDataForOccupation(occupation)), [dispatch]);

    let occupations : ILooseObject = {};
    if(occupationList) {
        occupations = {
            select: "Valitse ammattinimike...",
            ...occupationList
        };
    }

    const occupationSelectedHandler = (occupation : string ) => {
        setOccupation(occupation);
        if(occupation !== 'select') {
            if(!salaryDataCache[occupation]) {
                fetchSalaryData(occupation);
            }
        }
    }

    const occupationOptions = Object.keys(occupations).map((occ, index) => (
        <option key={occ} value={occ}>{occupations[occ]}</option>
    ));

    let salaryInfoContent : React.ReactNode = null;
    if(salaryDataCache[occupation]) {
        salaryInfoContent = (
            <React.Fragment>
                <SalarySummary 
                    occupation={occupations[occupation]}
                    data={salaryDataCache[occupation]} 
                />
                <SalaryCharts 
                    data={salaryDataCache[occupation]}
                />
                <SalaryProvinceTable 
                    data={salaryDataCache[occupation]}
                />
            </React.Fragment>
        );
    } else if(noData) {
        salaryInfoContent = (
            <div className={classes.NoData}>
                <div className={classes.NoDataParagraph}>Tälle ammattinimikkeelle ei ole vielä ilmoitettu palkkatietoja.</div>
                <div className={classes.NoDataParagraph}>Palvelu on äskettäin avattu. Voit lisätä omat palkkatietosi tästä:</div>
                <div className={classes.NoDataParagraph}>
                    <NavLink to='/submit-salary'>
                        <Button>Ilmoita palkkasi nimettömänä</Button>
                    </NavLink>
                </div>
            </div>
            )
    }

    return (
        <React.Fragment>
            <Header />
            <div className={['narrow-text-box', classes.SalarySearch, 'centered'].join(' ')}>
                <div className={classes.OccupationSelect}>
                    <Select 
                        value={occupation} 
                        onChange={(e : any) => occupationSelectedHandler(e.target.value)}
                        id='occupation'>
                        {occupationOptions}
                    </Select>
                    {fetching ? (
                        <div className={classes.Fetching}>
                            Haetaan palkkatietoja...
                        </div> 
                    ) : (
                        salaryInfoContent
                    )}
                </div>
            </div>
        </React.Fragment>
    );
}

export default SalarySearch;