export const median = (valuesIn : number[], comparator? : (a:number, b:number)=>number) : number => {

    let values = valuesIn;

    if(comparator) {
        values = [...valuesIn];
        values.sort( comparator );
    }
    var half = Math.floor( values.length / 2 );
    if( values.length % 2 ) {
        return values[half];
    } else {
        return ( values[half-1] + values[half] ) / 2.0;
    }
}

export interface ISalaryData {
    salary : number,
    province : string,
    experience : number,
    gender : string
}

const getStatsForProvince = (p : string, data : ISalaryData[]) => {
    let count = 0; 
    let medianSalary = 0;
    let minSal = 99999;
    let maxSal = 0;
    const salaries : number[] = [];
    data.forEach(sal => {
        if(p === '*' || sal.province === p) {
            minSal = Math.min(minSal, sal.salary);
            maxSal = Math.max(maxSal, sal.salary);
            ++count;
            salaries.push(sal.salary);
        }
    });
    if(minSal > 99998) {
        minSal = 0;
    }
    if(count > 0) {
        salaries.sort();
        medianSalary = median(salaries);
    }
    return {count: count, medianSalary: medianSalary, minSal: minSal, maxSal: maxSal};
};

export const getOccupationSalarySummaryData = (dataIn : ISalaryData[] ) => {

    let data = [...dataIn];

    let ret = {
        count: 0,
        salaryRange: '',
        medianSalary: 0
    };

    data.sort((a, b) => (a.salary > b.salary) ? 1 : -1);
    const stats = getStatsForProvince('*', data);

    ret.count = stats.count;
    ret.salaryRange = stats.minSal + ' € - ' + stats.maxSal + ' € / kk';
    ret.medianSalary = stats.medianSalary;

    return ret;
}

export const  getOccupationSalaryChartData = ( dataIn : ISalaryData[] ) => {

    let data = [...dataIn];

    data.sort((a, b) => (a.salary > b.salary) ? 1 : -1);
    const stats = getStatsForProvince('*', data);

    const fromK = Math.round(Math.floor(stats.minSal / 1000) * 1000);
    let toK = Math.round((Math.floor(stats.maxSal / 1000) + 1) * 1000);
    if((stats.maxSal - (toK - 1000)) < 100) {
        toK = Math.round(toK - 1000);
    }
    let division = 100;
    let labels = [];
    let buckets : number[] = [];
    for(let i = fromK; i <= (toK + 0.00001); i += division) {
        buckets.push(0);
        labels.push(Math.round(i) + '€');
    }
    data.forEach(sal => {
        const bucket = Math.min(Math.round((sal.salary - fromK) / division), buckets.length - 1);
        buckets[bucket] += 1;
    });
    let salaryPercentages : string[] = [];
    buckets.forEach((c, idx, arr) => {
        salaryPercentages.push(((c * 100.0) / stats.count).toFixed(2));
    });
    const cd = {
        labels: labels, 
        datasets: [
            {
                label: "Palkkajakauma", 
                backgroundColor: "rgb(1, 133, 194)", 
                data: salaryPercentages
            }
        ]
    };
    return cd;
}

export interface IProvinceTable {
    name : string,
    key: string,
    median: number,
    count: number
}

interface IIndexable {
    [key: string]: string;
  }

export const getOccupationSalaryProvinceTable = (dataIn : ISalaryData[], provinces : IIndexable) => {

    let data = [...dataIn];

    data.sort((a, b) => (a.province > b.province) ? 1 : -1);
    const provinceArray : string[] = [];
    data.forEach(sal => {
        if(provinceArray.indexOf(sal.province) < 0) {
            provinceArray.push(sal.province);
        }
    });
    const provinceTable : IProvinceTable[] = [];
    provinceArray.forEach(p => {
        const stat = getStatsForProvince(p, data);
        let provinceText : string = provinces[p];
        if(stat.count < 5) {
            provinceText = '-';
        }
        provinceTable.push({name: provinceText, key: provinces[p], median: stat.medianSalary, count: stat.count});
    });
    return provinceTable;
}